import socket

server_socket = socket.socket()
print("Socket successfully created")

ip = "10.32.201.170"
port = 12345

server_socket.bind((ip, port))
print("Socket binded to %s:%s" % (ip, port))

server_socket.listen(5)
print("Socket is listening")

file_path = r"D:\NetworkSocket\a.txt"
with open(file_path, "r") as file:
    file_contents = file.read()
    while True:
        client_socket, addr = server_socket.accept()
        print('Got connection from', addr)

        client_socket.send(file_contents.encode())

        client_socket.close()